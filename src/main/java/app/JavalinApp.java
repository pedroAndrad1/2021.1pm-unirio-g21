package app;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;
import static io.javalin.apibuilder.ApiBuilder.put;

import controllers.BicicletaController;
import controllers.TotemController;
import controllers.TrancaController;
import io.javalin.Javalin;

/**
 * Encapsula o Javalin. Aqui também serão definidas as rotas e os Controllers delas.
 * 
 * @author Pedro
 *
 */
public class JavalinApp {
	 
	private Javalin app = 
			// definindo que o retorno padrão é do tipo json
            Javalin.create(config -> config.defaultContentType = "application/json")
            	// definindo as rotas, seus tipos HTTPS, seus Controllers 
            	// e as funcoes que respondem a rota.
                .routes(() -> {
                    path("/bicicleta", () ->{
                    	 get(BicicletaController::getBicicletas);
                    	 post(BicicletaController::cadastrarBicicleta);
                    	 
                    	 path(":id", () -> {
                         	get(BicicletaController::getBicicletaById);
                         	put(BicicletaController::updateBicicleta);
                         	delete(BicicletaController::deleteBicicleta);
                         	
                         	path("status/:acao", () -> put(BicicletaController::updateStatusBicicleta));
                         });
                    	 
                    	path("integrarNaRede", ()-> post(BicicletaController::integrarNaRede));
                     	
                     	path("retirarDaRede", ()-> post(BicicletaController::retirarDaRede));
                    	 
                    });
                    path("/totem", () -> {
                    	get(TotemController::getTotens);
                    	post(TotemController::createTotem);
                    	
                    	path(":id", () ->{
                    		put(TotemController::updateTotem);
                    		delete(TotemController::deleteTotem);
                    		
                    		path("trancas", ()-> get(TotemController::getTrancasByTotem) );
                    		path("bicicletas", ()-> get(TotemController::getBicicletasByTotem) );
                    	});
                    	
                    	
                    });
                    path("/tranca", ()->{
                    	get(TrancaController::getTrancas);
                    	post(TrancaController::createTranca);
                    	
                    	path(":id", () ->{
                    		get(TrancaController::getTrancaById);
                    		delete(TrancaController::deleteTranca);
                    		
                    		path("status/:acao", () -> put(TrancaController::updateStatusTranca));
                    		path("bicicleta", () -> get(TrancaController::getBicicletaByTranca));
                    		
                    	});
                    	
                    	path("integrarNaRede", ()-> post(TrancaController::integrarNaRede));
                    	
                    	path("retirarDaRede", ()-> post(TrancaController::retirarDaRede));
                    	
                    });
                });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
