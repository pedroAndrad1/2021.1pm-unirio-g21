package app;

//Aqui o Javalin é iniciado. O Javalin tá incapsulado na classe JavalinApp

public class Main {
	
	
    public static void main(String[] args) {
        JavalinApp app = new JavalinApp();
        app.start(getHerokuAssignedPort());
    }
    
    /***
     * Se a app estiver rodando no ambiente do Heroku, esta function 
     * vai pegar a porta que a app vai rodar e retorna ela.
     * 
     * @return porta da app no Heroku
     */
    private static int getHerokuAssignedPort() {
        String herokuPort = System.getenv("PORT");
        if (herokuPort != null) {
          return Integer.parseInt(herokuPort);
        }
        return 7000;
      }
}
