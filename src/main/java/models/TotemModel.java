package models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TotemModel {
	
	private final UUID id;
	private String coordinates;
	private ArrayList<TrancaModel> trancas = new ArrayList<>();
	
	//Para fins de teste
	public TotemModel() {this.id = null;}
	
	public TotemModel(@JsonProperty(value= "coordinates", required = true) String coordinates) {
		this.id = UUID.randomUUID();
		this.coordinates = coordinates;
	}
	
	public void update(TotemModel newTotem) {
		this.coordinates = newTotem.coordinates;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public UUID getId() {
		return id;
	}
	
	public void addTranca(TrancaModel tranca) {
		trancas.add(tranca);
	}
	
	public void removeTranca(TrancaModel tranca) {
		trancas.remove(tranca);
	}
	
	public List<TrancaModel> getTrancas() {
		return trancas;
	}
	
	
}
