package models;

/**
 * Representa um Erro de requisição a ser devolvido.
 * @author Pedro de Andrade
 *
 */
public class Erro {
	private static Long erroId = 0L;
	
	private final Long id;
	private final Integer codigo;
	private final String mensagem;
	
	public Erro(
			Integer codigo, 
			String mensagem
	){
		this.id = erroId;
		this.codigo = codigo;
		this.mensagem = mensagem;
		
		erroId++;
	}

	public Long getId() {
		return id;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

	
	
	
	
}
