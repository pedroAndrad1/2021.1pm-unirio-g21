package models;

/**
 * Status possíveis de uma bicicleta
 * @author Pedro
 *
 */
public enum BicicletaStatus {
	DISPONIVEL, EM_USO, NOVA, APOSENTADA, REPARO_SOLICITADO, EM_REPARO
}
