package models;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Classe que representa uma bicicleta no banco
 * 
 * @author Pedro
 *
 */
public class BicicletaModel {
	
	private final UUID id;
	private String marca;
	private String modelo;
	private Integer ano;
	private Integer numero;
	private BicicletaStatus status;
	
	public BicicletaModel
	(
		@JsonProperty(value= "marca", required = true) String marca, 
		@JsonProperty(value= "modelo", required = true) String modelo, 
		@JsonProperty(value= "ano", required = true) Integer ano, 
		@JsonProperty(value= "numero", required = true) Integer numero
	){
		this.id = UUID.randomUUID();
		this.marca = marca;
		this.modelo = modelo;
		this.ano = ano;
		this.numero = numero;
		this.status = BicicletaStatus.NOVA;
	}
	
	//Para fins de teste
	public BicicletaModel() {this.id = null;}



	public void update(BicicletaModel update) {
		this.marca = update.marca;
		this.modelo = update.modelo;
		this.ano = update.ano;
		this.numero = update.numero;
		this.status = update.status;
	}

	public UUID getId() {
		return id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public BicicletaStatus getStatus() {
		return status;
	}

	public void setStatus(BicicletaStatus status) {
		this.status = status;
	}
	
	

}
