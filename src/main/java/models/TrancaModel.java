package models;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrancaModel {
	private final UUID id;
	private BicicletaModel bicicleta;
	private Integer numero;
	private String localizacao;
	private Integer anoDeFabricacao;
	private String modelo;
	private TrancaStatus status;
	
	
	public TrancaModel(
			@JsonProperty(value= "numero", required = true) Integer numero, 
			@JsonProperty(value= "localizacao", required = true) String localizacao, 
			@JsonProperty(value= "anoDeFabricacao", required = true) Integer anoDeFabricacao, 
			@JsonProperty(value= "modelo", required = true) String modelo, 
			@JsonProperty(value= "status", required = true) TrancaStatus status) {
		this.id = UUID.randomUUID();
		this.numero = numero;
		this.localizacao = localizacao;
		this.anoDeFabricacao = anoDeFabricacao;
		this.modelo = modelo;
		this.status = status;
	}
	

	public BicicletaModel getBicicleta() {
		return bicicleta;
	}
	public void setBicicleta(BicicletaModel bicicleta) {
		this.bicicleta = bicicleta;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public String getLocalizacao() {
		return localizacao;
	}
	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}
	public Integer getAnoDeFabricacao() {
		return anoDeFabricacao;
	}
	public void setAnoDeFabricacao(Integer anoDeFabricacao) {
		this.anoDeFabricacao = anoDeFabricacao;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public TrancaStatus getStatus() {
		return status;
	}
	public void setStatus(TrancaStatus status) {
		this.status = status;
	}
	public UUID getId() {
		return id;
	}

	public void addBicicleta(BicicletaModel bicicleta) {
		this.bicicleta = bicicleta;
	}
	
	public void removeBicicleta() {
		this.bicicleta = null;
	}
	
	
	
}
