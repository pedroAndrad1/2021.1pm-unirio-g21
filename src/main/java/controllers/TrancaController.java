package controllers;

import java.util.ArrayList;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.javalin.http.Context;
import models.AcoesDeRede;
import models.Erro;
import models.TotemModel;
import models.TrancaModel;
import models.TrancaStatus;

public final class TrancaController {
		
		private static final String MESSAGE_404 = "Não encontrado";
		private static final String MESSAGE_422 = "Dados inválidos";
		private static ArrayList<TrancaModel> trancas = new ArrayList<>();
		
		//tira codesmell
		private TrancaController() {}

		/**
		 * Cria ua tranca no ArrayList de trancas para fins de teste. Retorna o
		 * id dessa tranca, que foi posto na ultima posição do ArrayList
		 */
		public static UUID trancaMockId() {	
			trancas.add(new TrancaModel(2021, "0,0", 2021 , "teste", TrancaStatus.NOVA));
			return trancas.get(trancas.size() - 1).getId();

		}

		/**
		 * Cria uma tranca no ArrayList de trancas para fins de teste. Retorna o
		 * essa tranca, que foi posto na ultima posição do ArrayList
		 */
		public static TrancaModel trancaMock() {
			trancas.add(new TrancaModel(2021, "0,0", 2021 , "teste", TrancaStatus.NOVA));
			return trancas.get(trancas.size() - 1);

		}
		
		/**
		 * Cria uma tranca no ArrayList de trancas para fins de teste. Retorna o
		 * essa tranca, que foi posto na ultima posição do ArrayList. A tranca e
		 * passada por parametro.
		 */
		public static TrancaModel trancaMock(TrancaModel novaTranca) {
			trancas.add(novaTranca);
			return trancas.get(trancas.size() - 1);

		}
		
		
		
		public static void getTrancas(Context ctx) {
			ctx.status(200);
			ctx.json(trancas);
		}
		
		public static void getTrancaById(Context ctx) {	
			// Iterando pelo ArrayList de trancas para verificar se há a tranca
			// com o id buscado.
			for(TrancaModel tranca: trancas) {
				// Se tiver essa tranca entao retorna ela
				if(tranca.getId().equals(UUID.fromString(ctx.pathParam("id")))) {
					ctx.json(tranca);
					ctx.status(200);
					return;
				}
			}
			// Senao retorna um erro
			ctx.status(404);
			ctx.json(new Erro(404, MESSAGE_404));
		
		}
		
		public static void createTranca(Context ctx) {
			//Mapeando a string em formato de json para a TrancaModel
			TrancaModel novaTranca;
			try {
				novaTranca = new ObjectMapper().readValue(ctx.body(), TrancaModel.class);
				trancas.add(novaTranca);
				//retorna a tranca cadastrada
				ctx.status(200);
				ctx.json(novaTranca);
				
			} catch (JsonProcessingException e) {
				ctx.status(422);
				ctx.json(new Erro(422,MESSAGE_422));
			}
		}
		
		public static void deleteTranca(Context ctx) {
			for(int i = 0; i < trancas.size(); i++) {
				// Se tiver essa tranca entao apaga ela
				if(trancas.get(i).getId().equals(UUID.fromString(ctx.pathParam("id"))))  {
					trancas.remove(i);
					ctx.status(200);
					return;
				}
			}
			// Senao retorna um erro
			ctx.status(404);
			ctx.json(new Erro(404, MESSAGE_404));
		}
		
		public static void updateStatusTranca(Context ctx) {
			// Iterando pelo ArrayList de trancas para verificar se há a Tranca
			// com o id buscado.
			for(TrancaModel tranca: trancas) {
				// Se tiver essa Tranca entao retorna ela
				if(tranca.getId().equals(UUID.fromString(ctx.pathParam("id"))) ) {
					try {
						// Tenta coverter a String para um valor do ENUM de Status possiveis
						tranca.setStatus(TrancaStatus.valueOf(ctx.pathParam("acao")));
						ctx.status(200);
						ctx.json(tranca);
						// Se nao bater com nenhum dos valores, lanca uma exception
					} catch (IllegalArgumentException e) {
						ctx.status(422);
						ctx.json(new Erro(422,MESSAGE_422));
					}
					
					return;
				}
			}
			// Senao retorna um erro
			ctx.status(404);
			ctx.json(new Erro(404, MESSAGE_404));
		}

		public static TrancaModel getTrancaById(UUID id) {
			// Iterando pelo ArrayList de trancas para verificar se há a tranca
			// com o id buscado.
			for(TrancaModel tranca: trancas) {
				// Se tiver essa tranca entao retorna ela
				if(tranca.getId().equals(id)) {
					return tranca;
				}
			}
			// Senao retorna um erro
			return null;
					
		}
		
		
		/**
		 * Adiciona uma tranca a um totem
		 * @param ctx
		 */
		public static void integrarNaRede(Context ctx) {
			integrarOuRetirarDaRede(ctx, AcoesDeRede.INTEGRAR);
			
		}

		private static void integrarOuRetirarDaRede(Context ctx, AcoesDeRede acao) {
			//Nesse json tem as chaves idTotem e idTranca
			JsonNode jsonNode;
			try {
				jsonNode = parseRequestBodyToJson(ctx);
			} catch (JsonProcessingException e) {
				//Se der exception, os dados são inválidos
				ctx.status(422);
				ctx.json(new Erro(422, "Dados Inválidos (Tranca id ou Totem id inválido)"));
				return;
			}
			//pega o totem pelo id
			TrancaModel tranca = getTrancaById(UUID.fromString(jsonNode.get("idTranca").asText()));
			//pega a tranca pelo id
			TotemModel totem = TotemController.getTotemById(UUID.fromString(jsonNode.get("idTotem").asText()));
			//Se um deles for null, quer dizer que o Id passado está errado
			if(totem == null || tranca == null) {
				ctx.status(422);
				ctx.json(new Erro(422, "Dados Inválidos (Tranca id ou Totem id inválido)"));
			}
			else {
				//Se a tranca estiver aposentada ou em reparo, então os dados tambem estao invalidos
				if(tranca.getStatus() == TrancaStatus.APOSENTADA || tranca.getStatus() == TrancaStatus.EM_REPARO) {
					ctx.status(422);
					ctx.json(new Erro(422, "Dados Inválidos (Tranca aposentada ou em reparo)"));
				}
				else {
					if(acao.equals(AcoesDeRede.INTEGRAR)) {
						//Entao esta tudo certo e a tranca e adicionada no totem
						totem.addTranca(tranca);
						ctx.status(200);
					}
					else {
						//Entao esta tudo certo e a tranca e retirada do totem
						totem.removeTranca(tranca);
						ctx.status(200);
					}
				}
			}
		}
		
		/**
		 * Retira uma tranca de um totem
		 * @param ctx
		 */
		public static void retirarDaRede(Context ctx) {
			integrarOuRetirarDaRede(ctx, AcoesDeRede.RETIRAR);
		}
		
		/**
		 * Parseia a body do request (um json em formato de String) para um
		 * JsonNode do jackson.
		 * 
		 * @param ctx
		 * @return JsonNode
		 * @throws JsonProcessingException 
		 * @throws JsonMappingException 
		 */
		private static JsonNode parseRequestBodyToJson(Context ctx) throws JsonProcessingException {
			//Parseando o Json vindo da request em um JsonNode
			ObjectMapper objectMapper = new ObjectMapper();
			
			 return objectMapper.readTree(ctx.body());
		}
		
		public static void getBicicletaByTranca(Context ctx) {
			TrancaModel tranca = getTrancaById(UUID.fromString(ctx.pathParam("id")));
			//Tranca não existe
			if(tranca == null) {
				ctx.status(422);
				ctx.json(new Erro(422, "Id da tranca inválido"));
				return;
			}
			//Tranca sem bicicleta
			if(tranca.getBicicleta() == null) {
				ctx.status(404);
				ctx.json(new Erro(404, " Bicicleta não encontrada"));
				return;
			}
			//Tudo certo, retornando a bicicleta na tranca
			ctx.status(200);
			ctx.json(tranca.getBicicleta());
			
		}
}
