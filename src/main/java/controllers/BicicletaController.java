package controllers;

import java.util.ArrayList;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.javalin.http.Context;
import models.AcoesDeRede;
import models.BicicletaModel;
import models.BicicletaStatus;
import models.Erro;
import models.TrancaModel;
import models.TrancaStatus;

public final class BicicletaController {
	
	private static final String MESSAGE_422 = "Dados inválidos";
	//Constantes
	private static final String STRING_TESTE = "TESTE";
	private static final String MESSAGE_404 = "Não encontrado";

	private static ArrayList<BicicletaModel> bicicletas = new ArrayList<>();

	private BicicletaController() {}
	
	/**
	 * Cria uma bicicleta no ArrayList de bicicletas para fins de teste.
	 * Retorna o id dessa bicicleta, que foi posta na ultima posição do ArrayList
	 */
	public static UUID bicicletaMockId() {
		bicicletas.add(new BicicletaModel(STRING_TESTE, STRING_TESTE, 2021, 13));
		return bicicletas.get(bicicletas.size()-1).getId();
		
	}

	/**
	 * Cria uma bicicleta no ArrayList de bicicletas para fins de teste.
	 * Retorna o essa bicicleta, que foi posta na ultima posição do ArrayList
	 */
	public static BicicletaModel bicicletaMock() {
		bicicletas.add(new BicicletaModel(STRING_TESTE,STRING_TESTE, 2021, 13));
		return bicicletas.get(bicicletas.size()-1);
		
	}

	public static void getBicicletas(Context ctx) {
		ctx.json(bicicletas);
		ctx.status(200);
	}
	
	public static void cadastrarBicicleta(Context ctx) {
		try {
			//Mapeando a string em formato de json para a BicicletaModel
			BicicletaModel novaBicicleta = new ObjectMapper().readValue(ctx.body(), BicicletaModel.class);
			bicicletas.add(novaBicicleta);
			//retorna a bicicleta cadastrada
			ctx.status(200);
			ctx.json(novaBicicleta);

		} catch (Exception e) {
			ctx.status(422);
			ctx.json(new Erro(422,MESSAGE_422));

		}
	}
	
	public static void getBicicletaById(Context ctx) {	
		// Iterando pelo ArrayList de bicicletas para verificar se há a bicicleta
		// com o id buscado.
		for(BicicletaModel bicicleta: bicicletas) {
			// Se tiver essa bicicleta entao retorna ela
			if(bicicleta.getId().equals(UUID.fromString(ctx.pathParam("id")))) {
				ctx.json(bicicleta);
				ctx.status(200);
				return;
			}
		}
		// Senao retorna um erro
		ctx.status(404);
		ctx.json(new Erro(404, MESSAGE_404));
	
	}
	
	private static BicicletaModel getBicicletaById(UUID id) {
		// Iterando pelo ArrayList de bicicletas para verificar se há a bicicleta
		// com o id buscado.
		for(BicicletaModel bicicleta: bicicletas) {
			// Se tiver essa bicicleta entao retorna ela
			if(bicicleta.getId().equals(id)) {
				return bicicleta;
			}
		}
		// Senao retorna um null
		return null;
}
	
	
	public static void updateBicicleta(Context ctx) {
		//Mapeando a string em formato de json para a BicicletaModel
		try {
			BicicletaModel bicicletaUpdate = new ObjectMapper().readValue(ctx.body(), BicicletaModel.class);
			/// Iterando pelo ArrayList de bicicletas para verificar se há a bicicleta
			// com o id buscado para update
			for(BicicletaModel bicicleta: bicicletas) {
				// Se tiver essa bicicleta entao faz upadate e retorna ela
				if(bicicleta.getId().equals(UUID.fromString(ctx.pathParam("id")))) {
					bicicleta.update(bicicletaUpdate);
					ctx.json(bicicleta);
					ctx.status(200);
					return;
				}
			}
			// Senao retorna um erro
			ctx.status(404);
			ctx.json(new Erro(404, MESSAGE_404));
			
		} catch (Exception e) {
			ctx.status(422);
			ctx.json(new Erro(422,MESSAGE_422));
		}
	}
	
	public static void deleteBicicleta(Context ctx) {
		for(int i = 0; i < bicicletas.size(); i++) {
			// Se tiver essa bicicleta entao apaga ela
			if(bicicletas.get(i).getId().equals(UUID.fromString(ctx.pathParam("id"))))  {
				bicicletas.remove(i);
				ctx.status(200);
				return;
			}
		}
		// Senao retorna um erro
		ctx.status(404);
		ctx.json(new Erro(404, MESSAGE_404));
	}
	
	public static void updateStatusBicicleta(Context ctx) {
		// Iterando pelo ArrayList de bicicletas para verificar se há a bicicleta
		// com o id buscado.
		for(BicicletaModel bicicleta: bicicletas) {
			// Se tiver essa bicicleta entao retorna ela
			if(bicicleta.getId().equals(UUID.fromString(ctx.pathParam("id"))) ) {
				try {
					// Tenta coverter a String para um valor do ENUM de Status possiveis
					bicicleta.setStatus(BicicletaStatus.valueOf(ctx.pathParam("acao")));
					ctx.status(200);
					ctx.json(bicicleta);
				} catch (IllegalArgumentException e) {
					ctx.status(422);
					ctx.json(new Erro(422,MESSAGE_422));
				}
				
				return;
			}
			
			
		}
		// Senao retorna um erro
		ctx.status(404);
		ctx.json(new Erro(404, MESSAGE_404));
	}
	
	private static void integrarOuRetirarDaRede(Context ctx, AcoesDeRede acao) {
		//Nesse json tem as chaves idTranca e idBicicleta
		JsonNode jsonNode;
		try {
			jsonNode = parseRequestBodyToJson(ctx);
		} catch (JsonProcessingException e) {
			//Se der exception, os dados são inválidos
			ctx.status(422);
			ctx.json(new Erro(422, "Dados Inválidos (ex status inválido da bicicleta ou tranca)"));
			return;
		}
		//pega o totem pelo id
		TrancaModel tranca = TrancaController.getTrancaById(UUID.fromString(jsonNode.get("idTranca").asText()));
		//pega a tranca pelo id
		BicicletaModel bicicleta = getBicicletaById(UUID.fromString(jsonNode.get("idBicicleta").asText()));
		//Se um deles for null, quer dizer que o Id passado está errado
		if(bicicleta == null || tranca == null) {
			ctx.status(422);
			ctx.json(new Erro(422, "Dados Inválidos (Tranca id ou Bicicleta id inválido)"));
		}
		else {
			//Se a tranca OU a bicicleta estiver aposentada ou em reparo, então os dados tambem estao invalidos
			if(tranca.getStatus() == TrancaStatus.APOSENTADA || tranca.getStatus() == TrancaStatus.EM_REPARO ||
			   bicicleta.getStatus() == BicicletaStatus.APOSENTADA || bicicleta.getStatus() == BicicletaStatus.EM_REPARO
			) {
				ctx.status(422);
				ctx.json(new Erro(422, "Dados Inválidos (Bicicleta ou Tranca aposentada ou em reparo)"));
			}
			else {
				if(acao.equals(AcoesDeRede.INTEGRAR)) {
					//Entao esta tudo certo e a bicicleta e adicionada na tranca
					tranca.addBicicleta(bicicleta);
					ctx.status(200);
				}
				else {
					//Entao esta tudo certo e a bicicleta e retirada da tranca
					tranca.removeBicicleta();
					ctx.status(200);
				}
			}
		}
	}
	
	
	/**
	 * Retira uma bicicleta de uma tranca
	 * @param ctx
	 */
	public static void retirarDaRede(Context ctx) {
		integrarOuRetirarDaRede(ctx, AcoesDeRede.RETIRAR);
	}
	/**
	 * Integra uma bicicleta a uma tranca
	 * @param ctx
	 */
	public static void integrarNaRede(Context ctx) {
		integrarOuRetirarDaRede(ctx, AcoesDeRede.INTEGRAR);
	}
	

	/**
	 * Parseia a body do request (um json em formato de String) para um
	 * JsonNode do jackson.
	 * 
	 * @param ctx
	 * @return JsonNode
	 * @throws JsonProcessingException 
	 * @throws JsonMappingException 
	 */
	private static JsonNode parseRequestBodyToJson(Context ctx) throws JsonProcessingException {
		//Parseando o Json vindo da request em um JsonNode
		ObjectMapper objectMapper = new ObjectMapper();
		
		 return objectMapper.readTree(ctx.body());
	
	}
	
	
}
