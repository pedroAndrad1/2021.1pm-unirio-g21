package controllers;

import java.util.ArrayList;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.javalin.http.Context;
import models.BicicletaModel;
import models.Erro;
import models.TotemModel;
import models.TrancaModel;

public final class TotemController {
	// Constantes
	private static final String MESSAGE_422 = "Dados inválidos";
	private static final String COORDINATE_TESTE = "-22.951193, -43.180784";
	private static final String MESSAGE_404 = "Não encontrado";

	private static ArrayList<TotemModel> totens = new ArrayList<>();
	
	//tira codesmell
	private TotemController() {}

	/**
	 * Cria um totem no ArrayList de totens para fins de teste. Retorna o
	 * id desse totem, que foi posto na ultima posição do ArrayList
	 */
	public static UUID totemMockId() {
		totens.add(new TotemModel(COORDINATE_TESTE));
		return totens.get(totens.size() - 1).getId();

	}

	/**
	 * Cria um totem no ArrayList de totens para fins de teste. Retorna o
	 * esse totem, que foi posto na ultima posição do ArrayList
	 */
	public static TotemModel totemMock() {
		totens.add(new TotemModel(COORDINATE_TESTE));
		return totens.get(totens.size() - 1);

	}

	public static void getTotens(Context ctx) {
		ctx.status(200);
		ctx.json(totens);
	}
	
	static TotemModel getTotemById(UUID id) {
		for(TotemModel totem: totens) {
			if(totem.getId().equals(id)) {
				return totem;
			}
		}
		
		return null;
	}
	
	public static void createTotem(Context ctx) {
		//Mapeando a string em formato de json para a TotemModel
		TotemModel novoTotem;
		try {
			novoTotem = new ObjectMapper().readValue(ctx.body(), TotemModel.class);
			totens.add(novoTotem);
			//retorna o totem cadastrado
			ctx.status(200);
			ctx.json(novoTotem);
			
		} catch (JsonProcessingException e) {
			ctx.status(422);
			ctx.json(new Erro(422,MESSAGE_422));
		}
		
	}
	
	public static void updateTotem(Context ctx) {
		//Mapeando a string em formato de json para a TotemModel
		TotemModel updateTotem;
		try {
			updateTotem = new ObjectMapper().readValue(ctx.body(), TotemModel.class);
			/// Iterando pelo ArrayList de totens para verificar se há o totem
			// com o id buscado para update
			for(TotemModel totem: totens) {
				// Se tiver esse totem entao faz upadate e esse totem
				if(totem.getId().equals(UUID.fromString(ctx.pathParam("id")))) {
					totem.update(updateTotem);
					ctx.json(totem);
					ctx.status(200);
					return;
				}
			}
			// Senao retorna um erro
			ctx.status(404);
			ctx.json(new Erro(404, MESSAGE_404));
			
		} catch (JsonProcessingException e) {
			ctx.status(422);
			ctx.json(new Erro(422,MESSAGE_422));
		}
	}
	
	public static void deleteTotem(Context ctx) {
		for(int i = 0; i < totens.size(); i++) {
			// Se tiver essa bicicleta entao apaga ela
			if(totens.get(i).getId().equals(UUID.fromString(ctx.pathParam("id"))))  {
				totens.remove(i);
				ctx.status(200);
				return;
			}
		}
		// Senao retorna um erro
		ctx.status(404);
		ctx.json(new Erro(404, MESSAGE_404));
	}
	
	
	/**
	 * Retorna as trancas associadas a um totem
	 * @param ctx
	 */
	public static void getTrancasByTotem(Context ctx) {
		UUID totemId = null;
		try {
			totemId = UUID.fromString(ctx.pathParam("id"));
		}
		catch(IllegalArgumentException e) {
			ctx.status(422);
			ctx.json(new Erro(422, MESSAGE_422));
			return;
		}
		
		TotemModel totem = getTotemById(totemId);
		
		if(totem == null) {
			ctx.status(404);
			ctx.json(new Erro(404, MESSAGE_404));
		}
		else {
			ctx.status(200);
			ctx.json(totem.getTrancas());
		}
	}
	
	
	public static void getBicicletasByTotem(Context ctx) {
		TotemModel totem = null;
		try {
			 totem = getTotemById(UUID.fromString(ctx.pathParam("id")));
		}
		catch(IllegalArgumentException e) {
			ctx.status(422);
			ctx.json(new Erro(422, MESSAGE_422));
			return;
		}
		
		//Se nao tiver totem, entao o id esta errado
		if(totem == null) {
			ctx.status(404);
			ctx.json(new Erro(404, MESSAGE_404));
		}
		else {
			//Se tiver totem, entao retorna as bicicletas dele
			ctx.status(200);
			ctx.json(getBicicletasByTotem(totem));
			
		}
		
	}
	
	private static ArrayList<BicicletaModel> getBicicletasByTotem(TotemModel totem){
		ArrayList<BicicletaModel> bicicletas = new ArrayList<>();
		//Pegando todas as trancas e adicionando as bicicletas no ArrayList
		for(TrancaModel tranca: totem.getTrancas()) {
			if(tranca.getBicicleta() != null) {
				bicicletas.add(tranca.getBicicleta());
			}
		}
		
		return bicicletas;
	}
}
