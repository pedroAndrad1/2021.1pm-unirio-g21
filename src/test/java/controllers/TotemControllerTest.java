package controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import models.BicicletaModel;
import models.TotemModel;

class TotemControllerTest {
	private static JavalinApp app = new JavalinApp();
	private static String URL_BASE = "http://localhost:7000/";

	@BeforeAll
	static void init() {
		app.start(7000);
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void getTotensTest() {
		HttpResponse res = Unirest
				.get(URL_BASE + "totem")
				.asString();
		
		assertEquals(200, res.getStatus());
	}
	
	
	@Test
	void createTotemComDadosInvalidosTest() {
		HttpResponse res = Unirest
				.post(URL_BASE + "totem")
				.body("{}")
				.asString();
		assertEquals(422, res.getStatus());
	}
	
	@Test
	void createTotemTest() {
		HttpResponse res = Unirest
				.post(URL_BASE + "totem")
				.body("{\"coordinates\":\"0,0\"}")
				.asString();
		assertEquals(200, res.getStatus());
	}
	
	@Test
	void updateTotemTest() throws JsonMappingException, JsonProcessingException{
		//Adicionando um totem de teste para pegar um totem válido
		TotemModel totemTeste = TotemController.totemMock();
		//Fazendo update no totemTeste para testar dps
		totemTeste.setCoordinates("0,0");
		//Fazendo request de teste
		HttpResponse res = Unirest
				.put(URL_BASE + "totem/" +  totemTeste.getId())
				.body("{\"coordinates\":\"0,0\"}")
				.asString();
		assertEquals(200, res.getStatus());
		// Mapeando o objeto totemTeste para uma string no formato json
		// para comparacao com a resposta do put. Ambos tem que ser iguais
		String totemTesteJsonString;
		totemTesteJsonString = new ObjectMapper().writeValueAsString(totemTeste);
		
		assertEquals(totemTesteJsonString, res.getBody());
	}
	
	@Test
	void updateTotemComDadosErradosTest(){
		
		TotemModel totemTeste = new TotemModel();
		
		HttpResponse res1 = Unirest
				.put(URL_BASE + "totem/" + UUID.randomUUID())
				.body(totemTeste)
				.asString();
		assertEquals(422, res1.getStatus());
	}
	
	@Test
	void updateTotemComIdErradoTest() {
		TotemModel totemTeste = new TotemModel("0,0");
		HttpResponse res = Unirest
				.put(URL_BASE + "totem/" + UUID.randomUUID())
				.body(totemTeste)
				.asString();
		assertEquals(404, res.getStatus());
	}
	
	@Test
	void deleteTotem() {
		UUID totemTesteId = TotemController.totemMockId();
		
		HttpResponse res = Unirest
				.delete(URL_BASE +  "totem/" + totemTesteId)
				.asString();
		
		assertEquals(200, res.getStatus());
	}
	
	@Test
	void deleteTotemComIdErrado() {
		HttpResponse res = Unirest
				.delete(URL_BASE +  "totem/" + UUID.randomUUID())
				.asString();
		
		assertEquals(404, res.getStatus());
	}
	
	@Test
	void getTrancasByTotemTest() {		
		UUID totemTesteId = TotemController.totemMockId();
		
		HttpResponse res = Unirest
				.get(URL_BASE +  "totem/" + totemTesteId + "/trancas")
				.asString();
		
		assertEquals(200, res.getStatus());
	}
	
	@Test
	void getTrancasByTotemComIdInvalidoTest() {		
		HttpResponse res = Unirest
				.get(URL_BASE +  "totem/" + "ERRO" + "/trancas")
				.asString();
		
		assertEquals(422, res.getStatus());
	}
	
	@Test
	void getTrancasByTotemComIdErradoTest() {		
		HttpResponse res = Unirest
				.get(URL_BASE +  "totem/" + UUID.randomUUID() + "/trancas")
				.asString();
		
		assertEquals(404, res.getStatus());
	}
	
	@Test
	void getBicicletasByTotemTest() {		
		UUID totemTesteId = TotemController.totemMockId();
		
		HttpResponse res = Unirest
				.get(URL_BASE +  "totem/" + totemTesteId + "/bicicletas")
				.asString();
		
		assertEquals(200, res.getStatus());
	}
	
	@Test
	void getBicicletasByTotemComIdInvalidoTest() {		
		HttpResponse res = Unirest
				.get(URL_BASE +  "totem/" + "ERRO" + "/bicicletas")
				.asString();
		
		assertEquals(422, res.getStatus());
	}
	
	@Test
	void getBicicletasByTotemComIdErradoTest() {		
		HttpResponse res = Unirest
				.get(URL_BASE +  "totem/" + UUID.randomUUID() + "/bicicletas")
				.asString();
		
		assertEquals(404, res.getStatus());
	}
	
	
	
}
