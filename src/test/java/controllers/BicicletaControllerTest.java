package controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import models.AcoesDeRede;
import models.BicicletaModel;
import models.BicicletaStatus;
import models.TrancaModel;
import models.TrancaStatus;

class BicicletaControllerTest {
	private static JavalinApp app = new JavalinApp();
	private static String URL_BASE = "http://localhost:7010/";
	
	/**
	 * Parseia o body do HttpResponse para uma BicicletaModel
	 * @param res
	 * @return TrancaModel
	 */
	static BicicletaModel parseBicicletaModel(HttpResponse res) {
		//Parseando o retorno em json para uma string
		String myObj = res.getBody().toString();
		ObjectMapper objectMapper = new ObjectMapper();
		//Mapeando a string para um TrancaModel
		BicicletaModel bicicletaRetornada = null;
		try {
			bicicletaRetornada = objectMapper.readValue(myObj, BicicletaModel.class);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bicicletaRetornada;
	}

	@BeforeAll
	static void init() {
		app.start(7010);
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}

	@Test
	void getBicicletasTest() {
		HttpResponse response = Unirest.get("http://localhost:7010/bicicleta").asString();
		assertEquals(200, response.getStatus());
	}
	
	@Test
	void cadastrarBicicletaTest(){
		HttpResponse response = Unirest
				.post("http://localhost:7010/bicicleta")
				.body("{\"marca\":\"teste\", \"modelo\":\"teste\", \"ano\": 2021, \"numero\": 13 }")
				.asString();
		assertEquals(200, response.getStatus());
	}
	
	@Test
	void cadastrarBicicletaSemTodosOsParametrosTest(){
		HttpResponse response = Unirest
				.post("http://localhost:7010/bicicleta")
				.body("{\"marca\":\"teste\", \"modelo\":\"teste\", \"ano\": 2021 }")
				.asString();
		assertEquals(422, response.getStatus());
	}
	
	@Test
	void getBicicletaByIdTest() throws JsonMappingException, JsonProcessingException {
		//Adicionando uma bicicleta de teste para pegar um id válido
		UUID bicicletaTesteId = BicicletaController.bicicletaMockId();
		//Fazendo a request de teste
		HttpResponse res1 = Unirest
				.get("http://localhost:7010/bicicleta/" + bicicletaTesteId)
				.asString();
		assertEquals(200, res1.getStatus());
	}
	
	@Test
	void getBicicletaByIdErradoTest(){
		HttpResponse res1 = Unirest
				.get("http://localhost:7010/bicicleta/" + UUID.randomUUID())
				.asString();
		assertEquals(404, res1.getStatus());
	}
	
	@Test
	void updateBicicletaTest() throws JsonMappingException, JsonProcessingException{
		//Adicionando uma bicicleta de teste para pegar uma bicicleta válida
		BicicletaModel bicicletaTesteId = BicicletaController.bicicletaMock();
		//Fazendo update deste bicicleta
		bicicletaTesteId.setMarca("Nova marca");
		bicicletaTesteId.setModelo("Novo modelo");
		//Fazend request de teste
		HttpResponse res = Unirest
				.put("http://localhost:7010/bicicleta/" +  bicicletaTesteId.getId())
				.body(bicicletaTesteId)
				.asString();
		assertEquals(200, res.getStatus());
		// Mapeando o objeto bicicletaTesteId para uma string no formato json
		// para comparacao com a resposta do put. Ambos tem que ser iguais
		String novaBicicletaJsonString;
		novaBicicletaJsonString = new ObjectMapper().writeValueAsString(bicicletaTesteId);
		
		assertEquals(novaBicicletaJsonString, res.getBody());
	}
	
	@Test
	void updateBicicletaComDadosErradosTest(){
		
		BicicletaModel bicicletaTeste = new BicicletaModel();
		
		HttpResponse res1 = Unirest
				.put("http://localhost:7010/bicicleta/-1")
				.body(bicicletaTeste)
				.asString();
		assertEquals(422, res1.getStatus());
	}
	
	@Test
	void updateBicicletaComIdErradoTest() {
		BicicletaModel bicicletaTeste = new BicicletaModel("teste","teste", 2021, 13);
		HttpResponse res = Unirest
				.put("http://localhost:7010/bicicleta/" + UUID.randomUUID())
				.body(bicicletaTeste)
				.asString();
		assertEquals(404, res.getStatus());
	}
	
	@Test
	void deleteBicicleta() {
		UUID bicicletaTesteId = BicicletaController.bicicletaMockId();
		
		HttpResponse res = Unirest
				.delete("http://localhost:7010/bicicleta/" + bicicletaTesteId)
				.asString();
		
		assertEquals(200, res.getStatus());
	}
	
	@Test
	void deleteBicicletaComIdErrado() {
		HttpResponse res = Unirest
				.delete("http://localhost:7010/bicicleta/" + UUID.randomUUID())
				.asString();
		
		assertEquals(404, res.getStatus());
	}
	
	@Test
	void updateStatusBicicletaTest() {
		UUID bicicletaTesteId = BicicletaController.bicicletaMockId();
		
		HttpResponse res = Unirest
				.put(URL_BASE + "bicicleta/" + bicicletaTesteId + "/status/" + "EM_REPARO")
				.asString();
		
		assertEquals(200, res.getStatus());
		
		//Checando se a tranca retornada tem o status atualizado
		assertEquals(BicicletaStatus.EM_REPARO, parseBicicletaModel(res).getStatus());
	}
	
	@Test
	void updateStatusBicicletaTestComIdErrado() {
		HttpResponse res = Unirest
				.put(URL_BASE + "bicicleta/" + UUID.randomUUID() + "/status/" + "EM_REPARO")
				.asString();
		
		
		assertEquals(404, res.getStatus());
		
	}
	
	@Test
	void updateStatusBicicletaTestComAcaoErrada() {
		UUID bicicletaTesteId = BicicletaController.bicicletaMockId();
		
		HttpResponse res = Unirest
				.put(URL_BASE + "bicicleta/" + bicicletaTesteId + "/status/" + "ERRO")
				.asString();
		
		assertEquals(422, res.getStatus());
		
	}
	
	private int integrarOuRetirarDaRede(AcoesDeRede acao) {
		UUID idBicicleta = BicicletaController.bicicletaMockId();
		UUID idTranca = TrancaController.trancaMockId();
				
		JSONObject json = new JSONObject();
		json.put("idBicicleta", idBicicleta);
		json.put("idTranca", idTranca);
		
		
		HttpResponse res;
		if(acao.equals(AcoesDeRede.INTEGRAR)) {
			res = Unirest
					.post(URL_BASE + "bicicleta/integrarNaRede" )
					.body(json)
					.asString();
		}
		else {
			res = Unirest
					.post(URL_BASE + "bicicleta/retirarDaRede" )
					.body(json)
					.asString();
		}
		
		return res.getStatus();
	}
	
	
	private int integrarOuRetirarDaRedeComIdErrado(AcoesDeRede acao) {
		UUID idBicicleta = BicicletaController.bicicletaMockId();
				
		JSONObject json = new JSONObject();
		json.put("idBicicleta", idBicicleta);
		json.put("idTranca", UUID.randomUUID());
		
		HttpResponse res;
		if(acao.equals(AcoesDeRede.INTEGRAR)) {
			res = Unirest
					.post(URL_BASE + "bicicleta/integrarNaRede" )
					.body(json)
					.asString();
		}
		else {
			res = Unirest
					.post(URL_BASE + "bicicleta/retirarDaRede" )
					.body(json)
					.asString();
		}
		
		return res.getStatus();
	}
	
	
	private int integrarOuRetirarDaRedeComTrancaComStatusInvalido(AcoesDeRede acao) {
		UUID idBicicleta = BicicletaController.bicicletaMockId();
		TrancaModel tranca = TrancaController.trancaMock(new TrancaModel(2021, "0,0", 2021 , "teste", TrancaStatus.APOSENTADA));
		
		JSONObject json = new JSONObject();
		json.put("idBicicleta", idBicicleta);
		json.put("idTranca", tranca.getId());
		
		HttpResponse res;
		if(acao.equals(AcoesDeRede.INTEGRAR)) {
			res = Unirest
					.post(URL_BASE + "bicicleta/integrarNaRede" )
					.body(json)
					.asString();
		}
		else {
			res = Unirest
					.post(URL_BASE + "bicicleta/retirarDaRede" )
					.body(json)
					.asString();
		}
		
		return res.getStatus();
	}
	
	@Test
	void integrarNaRedeTest() {
		assertEquals(200,integrarOuRetirarDaRede(AcoesDeRede.INTEGRAR));
	}
	
	@Test
	void integrarNaRedeComIdErradoTest() {
		assertEquals(422,integrarOuRetirarDaRedeComIdErrado(AcoesDeRede.INTEGRAR));
	}
	
	@Test
	void integrarNaRedeRedeComStatusInvalidoTest() {
		assertEquals(422,integrarOuRetirarDaRedeComTrancaComStatusInvalido(AcoesDeRede.INTEGRAR));
	}
	
	@Test
	void retirarDaRedeTest() {
		assertEquals(200,integrarOuRetirarDaRede(AcoesDeRede.RETIRAR));
	}
	
	@Test
	void retirarDaRedeComIdErradoTest() {
		assertEquals(422,integrarOuRetirarDaRedeComIdErrado(AcoesDeRede.RETIRAR));
	}
	
	@Test
	void iretirarDaRedeRedeComStatusInvalidoTest() {
		assertEquals(422,integrarOuRetirarDaRedeComTrancaComStatusInvalido(AcoesDeRede.RETIRAR));
	}
	
}
