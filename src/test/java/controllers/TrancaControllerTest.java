package controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import models.AcoesDeRede;
import models.BicicletaModel;
import models.TrancaModel;
import models.TrancaStatus;

class TrancaControllerTest {
	private static JavalinApp app = new JavalinApp();
	private static String URL_BASE = "http://localhost:7020/";
	
	/**
	 * Parseia o body do HttpResponse para uma TrancaModel
	 * @param res
	 * @return TrancaModel
	 */
	private TrancaModel parseTrancaModel(HttpResponse res) {
		//Parseando o retorno em json para uma string
		String myObj = res.getBody().toString();
		ObjectMapper objectMapper = new ObjectMapper();
		//Mapeando a string para um TrancaModel
		TrancaModel trancaRetornada = null;
		try {
			trancaRetornada = objectMapper.readValue(myObj, TrancaModel.class);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return trancaRetornada;
	}

	@BeforeAll
	static void init() {
		app.start(7020);
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void getTrancasTest() {
		HttpResponse res = Unirest
				.get(URL_BASE + "tranca")
				.asString();
		
		assertEquals(200, res.getStatus());
	}
	
	@Test
	void getTrancaByIdTest() throws JsonMappingException, JsonProcessingException {
		//Adicionando uma bicicleta de teste para pegar um id válido
		UUID trancaTesteId = TrancaController.trancaMockId();
		//Fazendo a request de teste
		HttpResponse res = Unirest
				.get(URL_BASE + "tranca/" + trancaTesteId)
				.asString();
		assertEquals(200, res.getStatus());
		
		//Checando para ver se o retorno é o mesmo que busquei
		TrancaModel trancaRetornada = parseTrancaModel(res);
		
		assertEquals(trancaTesteId, trancaRetornada.getId());
	}
	
	@Test
	void getBicicletaByIdErradoTest(){
		HttpResponse res = Unirest
				.get(URL_BASE + "tranca/" + UUID.randomUUID())
				.asString();
		assertEquals(404, res.getStatus());
	}
	
	@Test
	void createTrancaTest() throws JsonMappingException, JsonProcessingException {				
		HttpResponse res = Unirest
				.post(URL_BASE + "tranca")
				.body("{\"numero\":2021, \"localizacao\":\"0,0\", \"anoDeFabricacao\": 2021, \"modelo\": \"teste\", \"status\": \"NOVA\" }")
				.asString();
		
		assertEquals(200, res.getStatus());
		
		//Criando objeto para checar se os dados que mandei batem com o que foi retornado
		TrancaModel trancaTeste = new TrancaModel(2021, "0,0", 2021, "teste", TrancaStatus.NOVA);
		
		TrancaModel trancaRetornada = parseTrancaModel(res);
		
		//Comparando o enviado com o retornado
		assertEquals(trancaTeste.getNumero(), trancaRetornada.getNumero());
		assertEquals(trancaTeste.getLocalizacao(), trancaRetornada.getLocalizacao());
		assertEquals(trancaTeste.getAnoDeFabricacao(), trancaRetornada.getAnoDeFabricacao());
		assertEquals(trancaTeste.getModelo(), trancaRetornada.getModelo());
		assertEquals(trancaTeste.getStatus(), trancaRetornada.getStatus());
	}
	
	@Test
	void createTrancaTestComDadosErrados() {
		HttpResponse res = Unirest
				.post(URL_BASE + "tranca")
				.body("{\"numero\":\"ERRO\", \"localizacao\":\"0,0\", \"anoDeFabricacao\": 2021, \"modelo\": \"teste\", \"status\": \"NOVA\" }")
				.asString();
		
		assertEquals(422, res.getStatus());
	}
	
	@Test
	void deleteTranca() {
		UUID trancaTesteId = TrancaController.trancaMockId();
		
		HttpResponse res = Unirest
				.delete(URL_BASE + "tranca/" + trancaTesteId)
				.asString();
		
		assertEquals(200, res.getStatus());
	}
	
	@Test
	void deleteBicicletaComIdErrado() {
		HttpResponse res = Unirest
				.delete(URL_BASE + "tranca/" + UUID.randomUUID())
				.asString();
		
		assertEquals(404, res.getStatus());
	}
	
	@Test
	void updateStatusTrancaTest() {
		UUID trancaTesteId = TrancaController.trancaMockId();
		
		HttpResponse res = Unirest
				.put(URL_BASE + "tranca/" + trancaTesteId + "/status/" + "EM_REPARO")
				.asString();
		
		assertEquals(200, res.getStatus());
		
		//Checando se a tranca retornada tem o status atualizado
		assertEquals(TrancaStatus.EM_REPARO, parseTrancaModel(res).getStatus());
	}
	
	@Test
	void updateStatusTrancaTestComIdErrado() {
		HttpResponse res = Unirest
				.put(URL_BASE + "tranca/" + UUID.randomUUID() + "/status/" + "EM_REPARO")
				.asString();
		
		
		assertEquals(404, res.getStatus());
		
	}
	
	@Test
	void updateStatusTrancaTestComAcaoErrada() {
		UUID trancaTesteId = TrancaController.trancaMockId();
		
		HttpResponse res = Unirest
				.put(URL_BASE + "tranca/" + trancaTesteId + "/status/" + "ERRO")
				.asString();
		
		assertEquals(422, res.getStatus());
		
	}
	
	
	private int integrarOuRetirarDaRede(AcoesDeRede acao) {
		UUID idTotem = TotemController.totemMockId();
		UUID idTranca = TrancaController.trancaMockId();
				
		JSONObject json = new JSONObject();
		json.put("idTotem", idTotem);
		json.put("idTranca", idTranca);
		
		
		HttpResponse res;
		if(acao.equals(AcoesDeRede.INTEGRAR)) {
			res = Unirest
					.post(URL_BASE + "tranca/integrarNaRede" )
					.body(json)
					.asString();
		}
		else {
			res = Unirest
					.post(URL_BASE + "tranca/retirarDaRede" )
					.body(json)
					.asString();
		}
		
		return res.getStatus();
	}
	
	
	private int integrarOuRetirarDaRedeComIdErrado(AcoesDeRede acao) {
		UUID idTotem = TotemController.totemMockId();
				
		JSONObject json = new JSONObject();
		json.put("idTotem", idTotem);
		json.put("idTranca", UUID.randomUUID());
		
		HttpResponse res;
		if(acao.equals(AcoesDeRede.INTEGRAR)) {
			res = Unirest
					.post(URL_BASE + "tranca/integrarNaRede" )
					.body(json)
					.asString();
		}
		else {
			res = Unirest
					.post(URL_BASE + "tranca/retirarDaRede" )
					.body(json)
					.asString();
		}
		
		return res.getStatus();
	}
	
	
	private int integrarOuRetirarDaRedeComTrancaComStatusInvalido(AcoesDeRede acao) {
		UUID idTotem = TotemController.totemMockId();
		TrancaModel tranca = TrancaController.trancaMock(new TrancaModel(2021, "0,0", 2021 , "teste", TrancaStatus.APOSENTADA));
		
		JSONObject json = new JSONObject();
		json.put("idTotem", idTotem);
		json.put("idTranca", tranca.getId());
		
		HttpResponse res;
		if(acao.equals(AcoesDeRede.INTEGRAR)) {
			res = Unirest
					.post(URL_BASE + "tranca/integrarNaRede" )
					.body(json)
					.asString();
		}
		else {
			res = Unirest
					.post(URL_BASE + "tranca/retirarDaRede" )
					.body(json)
					.asString();
		}
		
		return res.getStatus();
	}
	
	@Test
	void integrarNaRedeTest() {
		assertEquals(200,integrarOuRetirarDaRede(AcoesDeRede.INTEGRAR));
	}
	
	@Test
	void integrarNaRedeComIdErradoTest() {
		assertEquals(422,integrarOuRetirarDaRedeComIdErrado(AcoesDeRede.INTEGRAR));
	}
	
	@Test
	void integrarNaRedeRedeComStatusInvalidoTest() {
		assertEquals(422,integrarOuRetirarDaRedeComTrancaComStatusInvalido(AcoesDeRede.INTEGRAR));
	}
	
	@Test
	void retirarDaRedeTest() {
		assertEquals(200,integrarOuRetirarDaRede(AcoesDeRede.RETIRAR));
	}
	
	@Test
	void retirarDaRedeComIdErradoTest() {
		assertEquals(422,integrarOuRetirarDaRedeComIdErrado(AcoesDeRede.RETIRAR));
	}
	
	@Test
	void retirarDaRedeComStatusInvalidoTest() {
		assertEquals(422,integrarOuRetirarDaRedeComTrancaComStatusInvalido(AcoesDeRede.RETIRAR));
	}
	
	@Test
	void getBicicletaByTranca() {
		//Mockando uma tranca
		TrancaModel tranca = TrancaController.trancaMock();
		//Mockando uma bicicleta
		BicicletaModel bicicleta = BicicletaController.bicicletaMock();
		//Adicionando a bicicleta na tranca
		tranca.addBicicleta(bicicleta);
		//Como tranca e bicicleta sao referencias. Agora deve existir uma tranca
		//com uma bicicleta associada no sistema. Embora a tranca nao esteja associada
		//a um totem
		HttpResponse res = Unirest
				.get(URL_BASE + "tranca/" + tranca.getId() + "/bicicleta" )
				.asString();
		
		assertEquals(200, res.getStatus());
		
		//Parseando a resposta para uma bicicleta
		BicicletaModel bicicletaRetornada = BicicletaControllerTest.parseBicicletaModel(res);
		//Comparando id da bicicleta mockada com a bicicleta retornada
		assertEquals(bicicleta.getId(), bicicletaRetornada.getId());
	}
	
	
	
}
